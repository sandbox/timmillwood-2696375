<?php

namespace Drupal\workspace_sync;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\workspace\ReplicatorManager;

class Syncer {

  /** @var ReplicatorManager */
  protected $replicatorManager;

  /** @var EntityTypeManagerInterface  */
  protected $entityTypeManager;

  public function __construct(ReplicatorManager $replicator_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->replicatorManager = $replicator_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  public function sync(EntityInterface $entity) {
    if ($entity instanceof ContentEntityInterface) {
      /** @var \Drupal\multiversion\Entity\WorkspaceInterface $source_workspace */
      $source_workspace = $entity->get('workspace')->entity;
      $source_pointers = $this->entityTypeManager
        ->getStorage('workspace_pointer')
        ->loadByProperties(['workspace_pointer' => $source_workspace->id()]);
      foreach ($source_pointers as $source_pointer) {
        $target_workspace = $this->entityTypeManager
          ->getStorage('workspace')
          ->loadByProperties(['upstream' => $source_pointer->id()]);
        $target_pointers = $this->entityTypeManager
          ->getStorage('workspace_pointer')
          ->loadByProperties(['workspace_pointer' => $target_workspace->id()]);
        foreach ($target_pointers as $target_pointer) {
          $replication_log = $this->replicatorManager->update($source_pointer, $target_pointer);
          if ($replication_log->get('ok')) {
            drupal_set_message('Updated child workspaces');
          }
          else {
            drupal_set_message('Error updated child workspaces');
          }
        }
      }
    }
  }
}